package com.mycompany.o7spring5restjpamysqlbe;

import org.junit.Test;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan(value = "com.mycompany.o7spring5restjpamysqlbe")
@EnableJpaRepositories(value = "com.mycompany.o7spring5restjpamysqlbe.repository")
@EntityScan(basePackages = {"com.mycompany.o7spring5restjpamysqlbe.entity"})
@DataJpaTest
public class O7Spring5RestJpaMysqlBeApplicationTests {

	@Test
	public void contextLoads() {
	}

}
