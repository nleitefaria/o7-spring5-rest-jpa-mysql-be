package com.mycompany.o7spring5restjpamysqlbe.service;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.mycompany.o7spring5restjpamysqlbe.O7Spring5RestJpaMysqlBeApplicationTests;
import com.mycompany.o7spring5restjpamysqlbe.entity.Branch;
import com.mycompany.o7spring5restjpamysqlbe.entity.Product;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = O7Spring5RestJpaMysqlBeApplicationTests.class)
@ActiveProfiles("test")
public class BranchServiceTest 
{
	@Autowired
	BranchService branchService;
	
	@Test
	public void findOneBranchTest() 
	{
		Branch b1 = new Branch(1, "b1");
		branchService.save(b1);				
		assertEquals(new Long(1), branchService.count());			
		branchService.deleteAll();		
	}
	
	@Test
	public void findAllBranchesTest() 
	{
		Branch b1 = new Branch(1, "b1");
		branchService.save(b1);		
		Branch b2 = new Branch(2, "b2");
		branchService.save(b2);			
		List<Branch> pList = branchService.findAll();
		assertEquals(2, pList.size());
		branchService.deleteAll();	
	}
	
	@Test
	public void saveBranchTest() 
	{
		Branch b1 = new Branch(1, "b1");
		branchService.save(b1);				
		List<Branch> pList = branchService.findAll();
		assertEquals(1, pList.size());
		branchService.deleteAll();		
	}

}
