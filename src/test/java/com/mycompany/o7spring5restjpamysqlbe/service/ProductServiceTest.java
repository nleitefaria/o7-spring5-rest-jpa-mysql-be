package com.mycompany.o7spring5restjpamysqlbe.service;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.mycompany.o7spring5restjpamysqlbe.O7Spring5RestJpaMysqlBeApplicationTests;
import com.mycompany.o7spring5restjpamysqlbe.entity.Product;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = O7Spring5RestJpaMysqlBeApplicationTests.class)
@ActiveProfiles("test")
public class ProductServiceTest 
{
	@Autowired
	ProductService productService;
	
	@Test
	public void findOneProductTest() 
	{
		Product p1 = new Product("p1", "prod1");
		productService.save(p1);				
		assertEquals(new Long(1), productService.count());			
		productService.deleteAll();		
	}
	
	@Test
	public void findAllProductsTest() 
	{
		Product p1 = new Product("p1", "prod1");
		productService.save(p1);		
		Product p2 = new Product("p2", "prod2");
		productService.save(p2);		
		List<Product> pList = productService.findAll();
		assertEquals(2, pList.size());
		productService.deleteAll();	
	}
	
	@Test
	public void saveProductTest() 
	{
		Product p1 = new Product("p1", "prod1");
		Product pSaved = productService.save(p1);
		assertEquals("p1", pSaved.getProductCd());
		assertEquals("prod1", pSaved.getName());
		productService.deleteAll();	
	}
	
	@Test
	public void editProductTest() 
	{
		Product p1 = new Product("p1", "prod1");
		Product pSaved = productService.save(p1);
		pSaved.setName("prod11");
		Product pEdited = productService.edit("p1", pSaved);
		assertEquals("prod11", pEdited.getName());	
		productService.deleteAll();	
	}


}
