/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.o7spring5restjpamysqlbe.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author nleit_000
 */
@Entity
@Table(name = "individual")
@NamedQueries({
    @NamedQuery(name = "Individual.findAll", query = "SELECT i FROM Individual i")
    , @NamedQuery(name = "Individual.findByBirthDate", query = "SELECT i FROM Individual i WHERE i.birthDate = :birthDate")
    , @NamedQuery(name = "Individual.findByFirstName", query = "SELECT i FROM Individual i WHERE i.firstName = :firstName")
    , @NamedQuery(name = "Individual.findByLastName", query = "SELECT i FROM Individual i WHERE i.lastName = :lastName")
    , @NamedQuery(name = "Individual.findByCustId", query = "SELECT i FROM Individual i WHERE i.custId = :custId")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Individual implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @Column(name = "CUST_ID")
    private Integer custId;
    
    @Column(name = "BIRTH_DATE")
    @Temporal(TemporalType.DATE)
    private Date birthDate;
    
    @Basic(optional = false)
    @Column(name = "FIRST_NAME")
    private String firstName;
    
    @Basic(optional = false)
    @Column(name = "LAST_NAME")
    private String lastName;
    
    @JoinColumn(name = "CUST_ID", referencedColumnName = "CUST_ID", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Customer customer;

    public Individual() {
    }

    public Individual(Integer custId) {
        this.custId = custId;
    }

    public Individual(Integer custId, String firstName, String lastName) {
        this.custId = custId;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getCustId() {
        return custId;
    }

    public void setCustId(Integer custId) {
        this.custId = custId;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (custId != null ? custId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Individual)) {
            return false;
        }
        Individual other = (Individual) object;
        if ((this.custId == null && other.custId != null) || (this.custId != null && !this.custId.equals(other.custId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.myapp.entity.Individual[ custId=" + custId + " ]";
    }
    
}
