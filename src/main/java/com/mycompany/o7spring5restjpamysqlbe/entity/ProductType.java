/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.o7spring5restjpamysqlbe.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author nleit_000
 */
@Entity
@Table(name = "product_type")
@NamedQueries({
    @NamedQuery(name = "ProductType.findAll", query = "SELECT p FROM ProductType p")
    , @NamedQuery(name = "ProductType.findByProductTypeCd", query = "SELECT p FROM ProductType p WHERE p.productTypeCd = :productTypeCd")
    , @NamedQuery(name = "ProductType.findByName", query = "SELECT p FROM ProductType p WHERE p.name = :name")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ProductType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PRODUCT_TYPE_CD")
    private String productTypeCd;
    
    @Column(name = "NAME")
    private String name;
       
    @JsonIgnore
    @OneToMany(mappedBy = "productTypeCd")
    private List<Product> productList;

    public ProductType() {
    }

    public ProductType(String productTypeCd) {
        this.productTypeCd = productTypeCd;
    }

    public String getProductTypeCd() {
        return productTypeCd;
    }

    public void setProductTypeCd(String productTypeCd) {
        this.productTypeCd = productTypeCd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productTypeCd != null ? productTypeCd.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductType)) {
            return false;
        }
        ProductType other = (ProductType) object;
        if ((this.productTypeCd == null && other.productTypeCd != null) || (this.productTypeCd != null && !this.productTypeCd.equals(other.productTypeCd))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.myapp.entity.ProductType[ productTypeCd=" + productTypeCd + " ]";
    }
    
}
