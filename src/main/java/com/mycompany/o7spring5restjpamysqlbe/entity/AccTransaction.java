/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.o7spring5restjpamysqlbe.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author nleit_000
 */
@Entity
@Table(name = "acc_transaction")
@NamedQueries({
    @NamedQuery(name = "AccTransaction.findAll", query = "SELECT a FROM AccTransaction a")
    , @NamedQuery(name = "AccTransaction.findByTxnId", query = "SELECT a FROM AccTransaction a WHERE a.txnId = :txnId")
    , @NamedQuery(name = "AccTransaction.findByAmount", query = "SELECT a FROM AccTransaction a WHERE a.amount = :amount")
    , @NamedQuery(name = "AccTransaction.findByFundsAvailDate", query = "SELECT a FROM AccTransaction a WHERE a.fundsAvailDate = :fundsAvailDate")
    , @NamedQuery(name = "AccTransaction.findByTxnDate", query = "SELECT a FROM AccTransaction a WHERE a.txnDate = :txnDate")
    , @NamedQuery(name = "AccTransaction.findByTxnTypeCd", query = "SELECT a FROM AccTransaction a WHERE a.txnTypeCd = :txnTypeCd")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AccTransaction implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "TXN_ID")  
    private Long txnId;
    
    @Basic(optional = false)
    @Column(name = "AMOUNT")   
    private float amount;
    
    @Basic(optional = false)
    @Column(name = "FUNDS_AVAIL_DATE")
    @Temporal(TemporalType.TIMESTAMP)   
    private Date fundsAvailDate;
    
    @Basic(optional = false)
    @Column(name = "TXN_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date txnDate;
    
    @Column(name = "TXN_TYPE_CD")
    @JsonIgnore
    private String txnTypeCd;
    
    @JoinColumn(name = "ACCOUNT_ID", referencedColumnName = "ACCOUNT_ID")
    @ManyToOne
    private Account accountId;
    
    @JoinColumn(name = "EXECUTION_BRANCH_ID", referencedColumnName = "BRANCH_ID")
    @ManyToOne
    @JsonIgnore
    private Branch executionBranchId;
    
    @JoinColumn(name = "TELLER_EMP_ID", referencedColumnName = "EMP_ID")
    @ManyToOne
    @JsonIgnore
    private Employee tellerEmpId;

    public AccTransaction() {
    }

    public AccTransaction(Long txnId) {
        this.txnId = txnId;
    }

    public AccTransaction(Long txnId, float amount, Date fundsAvailDate, Date txnDate) {
        this.txnId = txnId;
        this.amount = amount;
        this.fundsAvailDate = fundsAvailDate;
        this.txnDate = txnDate;
    }

    public Long getTxnId() {
        return txnId;
    }

    public void setTxnId(Long txnId) {
        this.txnId = txnId;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public Date getFundsAvailDate() {
        return fundsAvailDate;
    }

    public void setFundsAvailDate(Date fundsAvailDate) {
        this.fundsAvailDate = fundsAvailDate;
    }

    public Date getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(Date txnDate) {
        this.txnDate = txnDate;
    }

    public String getTxnTypeCd() {
        return txnTypeCd;
    }

    public void setTxnTypeCd(String txnTypeCd) {
        this.txnTypeCd = txnTypeCd;
    }

    public Account getAccountId() {
        return accountId;
    }

    public void setAccountId(Account accountId) {
        this.accountId = accountId;
    }

    public Branch getExecutionBranchId() {
        return executionBranchId;
    }

    public void setExecutionBranchId(Branch executionBranchId) {
        this.executionBranchId = executionBranchId;
    }

    public Employee getTellerEmpId() {
        return tellerEmpId;
    }

    public void setTellerEmpId(Employee tellerEmpId) {
        this.tellerEmpId = tellerEmpId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (txnId != null ? txnId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccTransaction)) {
            return false;
        }
        AccTransaction other = (AccTransaction) object;
        if ((this.txnId == null && other.txnId != null) || (this.txnId != null && !this.txnId.equals(other.txnId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.myapp.entity.AccTransaction[ txnId=" + txnId + " ]";
    }
    
}
