/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.o7spring5restjpamysqlbe.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author nleit_000
 */
@Entity
@Table(name = "branch")
@NamedQueries({
    @NamedQuery(name = "Branch.findAll", query = "SELECT b FROM Branch b")
    , @NamedQuery(name = "Branch.findByBranchId", query = "SELECT b FROM Branch b WHERE b.branchId = :branchId")
    , @NamedQuery(name = "Branch.findByAddress", query = "SELECT b FROM Branch b WHERE b.address = :address")
    , @NamedQuery(name = "Branch.findByCity", query = "SELECT b FROM Branch b WHERE b.city = :city")
    , @NamedQuery(name = "Branch.findByName", query = "SELECT b FROM Branch b WHERE b.name = :name")
    , @NamedQuery(name = "Branch.findByState", query = "SELECT b FROM Branch b WHERE b.state = :state")
    , @NamedQuery(name = "Branch.findByZipCode", query = "SELECT b FROM Branch b WHERE b.zipCode = :zipCode")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Branch implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "BRANCH_ID")
    private Integer branchId;
    
    @Column(name = "ADDRESS")
    private String address;
    
    @Column(name = "CITY")
    private String city;
    
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    
    @Column(name = "STATE")
    private String state;
    
    @Column(name = "ZIP_CODE")
    private String zipCode;
        
    @OneToMany(mappedBy = "executionBranchId")
    private List<AccTransaction> accTransactionList;
       
    @OneToMany(mappedBy = "assignedBranchId")
    private List<Employee> employeeList;
       
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "openBranchId")
    @JsonIgnore
    private List<Account> accountList;

    public Branch() {
    }

    public Branch(Integer branchId) {
        this.branchId = branchId;
    }

    public Branch(Integer branchId, String name) {
        this.branchId = branchId;
        this.name = name;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public List<AccTransaction> getAccTransactionList() {
        return accTransactionList;
    }

    public void setAccTransactionList(List<AccTransaction> accTransactionList) {
        this.accTransactionList = accTransactionList;
    }

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    public List<Account> getAccountList() {
        return accountList;
    }

    public void setAccountList(List<Account> accountList) {
        this.accountList = accountList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (branchId != null ? branchId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Branch)) {
            return false;
        }
        Branch other = (Branch) object;
        if ((this.branchId == null && other.branchId != null) || (this.branchId != null && !this.branchId.equals(other.branchId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.myapp.entity.Branch[ branchId=" + branchId + " ]";
    }
    
}
