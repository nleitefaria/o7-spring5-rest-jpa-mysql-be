/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.o7spring5restjpamysqlbe.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author nleit_000
 */
@Entity
@Table(name = "business")
@NamedQueries({
    @NamedQuery(name = "Business.findAll", query = "SELECT b FROM Business b")
    , @NamedQuery(name = "Business.findByIncorpDate", query = "SELECT b FROM Business b WHERE b.incorpDate = :incorpDate")
    , @NamedQuery(name = "Business.findByName", query = "SELECT b FROM Business b WHERE b.name = :name")
    , @NamedQuery(name = "Business.findByStateId", query = "SELECT b FROM Business b WHERE b.stateId = :stateId")
    , @NamedQuery(name = "Business.findByCustId", query = "SELECT b FROM Business b WHERE b.custId = :custId")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Business implements Serializable 
{
    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @Column(name = "CUST_ID")
    private Integer custId;
    
    @Column(name = "INCORP_DATE")
    @Temporal(TemporalType.DATE)
    private Date incorpDate;
    
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    
    @Basic(optional = false)
    @Column(name = "STATE_ID")
    private String stateId;
        
    @JoinColumn(name = "CUST_ID", referencedColumnName = "CUST_ID", insertable = false, updatable = false)
    @OneToOne(optional = false)
    @JsonIgnore
    private Customer customer;

    public Business() {
    }

    public Business(Integer custId) {
        this.custId = custId;
    }

    public Business(Integer custId, String name, String stateId) {
        this.custId = custId;
        this.name = name;
        this.stateId = stateId;
    }

    public Date getIncorpDate() {
        return incorpDate;
    }

    public void setIncorpDate(Date incorpDate) {
        this.incorpDate = incorpDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public Integer getCustId() {
        return custId;
    }

    public void setCustId(Integer custId) {
        this.custId = custId;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (custId != null ? custId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Business)) {
            return false;
        }
        Business other = (Business) object;
        if ((this.custId == null && other.custId != null) || (this.custId != null && !this.custId.equals(other.custId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.myapp.entity.Business[ custId=" + custId + " ]";
    }
    
}
