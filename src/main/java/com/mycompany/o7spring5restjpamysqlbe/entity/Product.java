/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.o7spring5restjpamysqlbe.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author nleit_000
 */
@Entity
@Table(name = "product")
@NamedQueries({
    @NamedQuery(name = "Product.findAll", query = "SELECT p FROM Product p")
    , @NamedQuery(name = "Product.findByProductCd", query = "SELECT p FROM Product p WHERE p.productCd = :productCd")
    , @NamedQuery(name = "Product.findByDateOffered", query = "SELECT p FROM Product p WHERE p.dateOffered = :dateOffered")
    , @NamedQuery(name = "Product.findByDateRetired", query = "SELECT p FROM Product p WHERE p.dateRetired = :dateRetired")
    , @NamedQuery(name = "Product.findByName", query = "SELECT p FROM Product p WHERE p.name = :name")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @Column(name = "PRODUCT_CD")    
    private String productCd;
    
    @Column(name = "DATE_OFFERED")
    @Temporal(TemporalType.DATE)
    private Date dateOffered;
    
    @Column(name = "DATE_RETIRED")
    @Temporal(TemporalType.DATE)
    private Date dateRetired;
    
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    
    //@JsonIgnore
    @JoinColumn(name = "PRODUCT_TYPE_CD", referencedColumnName = "PRODUCT_TYPE_CD")
    @ManyToOne
    private ProductType productTypeCd;
    
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productCd")
    private List<Account> accountList;

    public Product() {
    }

    public Product(String productCd) {
        this.productCd = productCd;
    }

    public Product(String productCd, String name) {
        this.productCd = productCd;
        this.name = name;
    }

    public String getProductCd() {
        return productCd;
    }

    public void setProductCd(String productCd) {
        this.productCd = productCd;
    }

    public Date getDateOffered() {
        return dateOffered;
    }

    public void setDateOffered(Date dateOffered) {
        this.dateOffered = dateOffered;
    }

    public Date getDateRetired() {
        return dateRetired;
    }

    public void setDateRetired(Date dateRetired) {
        this.dateRetired = dateRetired;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductType getProductTypeCd() {
        return productTypeCd;
    }

    public void setProductTypeCd(ProductType productTypeCd) {
        this.productTypeCd = productTypeCd;
    }

    public List<Account> getAccountList() {
        return accountList;
    }

    public void setAccountList(List<Account> accountList) {
        this.accountList = accountList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productCd != null ? productCd.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Product)) {
            return false;
        }
        Product other = (Product) object;
        if ((this.productCd == null && other.productCd != null) || (this.productCd != null && !this.productCd.equals(other.productCd))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.myapp.entity.Product[ productCd=" + productCd + " ]";
    }
    
}
