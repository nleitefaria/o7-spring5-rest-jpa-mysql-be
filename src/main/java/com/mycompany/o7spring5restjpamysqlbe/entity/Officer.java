/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.o7spring5restjpamysqlbe.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author nleit_000
 */
@Entity
@Table(name = "officer")
@NamedQueries({
    @NamedQuery(name = "Officer.findAll", query = "SELECT o FROM Officer o")
    , @NamedQuery(name = "Officer.findByOfficerId", query = "SELECT o FROM Officer o WHERE o.officerId = :officerId")
    , @NamedQuery(name = "Officer.findByEndDate", query = "SELECT o FROM Officer o WHERE o.endDate = :endDate")
    , @NamedQuery(name = "Officer.findByFirstName", query = "SELECT o FROM Officer o WHERE o.firstName = :firstName")
    , @NamedQuery(name = "Officer.findByLastName", query = "SELECT o FROM Officer o WHERE o.lastName = :lastName")
    , @NamedQuery(name = "Officer.findByStartDate", query = "SELECT o FROM Officer o WHERE o.startDate = :startDate")
    , @NamedQuery(name = "Officer.findByTitle", query = "SELECT o FROM Officer o WHERE o.title = :title")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Officer implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "OFFICER_ID")
    private Integer officerId;
    
    @Column(name = "END_DATE")
    @Temporal(TemporalType.DATE)
    private Date endDate;
    
    @Basic(optional = false)
    @Column(name = "FIRST_NAME")
    private String firstName;
    @Basic(optional = false)
    @Column(name = "LAST_NAME")
    private String lastName;
    
    @Basic(optional = false)
    @Column(name = "START_DATE")
    @Temporal(TemporalType.DATE)
    private Date startDate;
    
    @Column(name = "TITLE")
    private String title;
    
    @JsonIgnore
    @JoinColumn(name = "CUST_ID", referencedColumnName = "CUST_ID")
    @ManyToOne
    private Customer custId;

    public Officer() {
    }

    public Officer(Integer officerId) {
        this.officerId = officerId;
    }

    public Officer(Integer officerId, String firstName, String lastName, Date startDate) {
        this.officerId = officerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.startDate = startDate;
    }

    public Integer getOfficerId() {
        return officerId;
    }

    public void setOfficerId(Integer officerId) {
        this.officerId = officerId;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Customer getCustId() {
        return custId;
    }

    public void setCustId(Customer custId) {
        this.custId = custId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (officerId != null ? officerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Officer)) {
            return false;
        }
        Officer other = (Officer) object;
        if ((this.officerId == null && other.officerId != null) || (this.officerId != null && !this.officerId.equals(other.officerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.myapp.entity.Officer[ officerId=" + officerId + " ]";
    }
    
}
