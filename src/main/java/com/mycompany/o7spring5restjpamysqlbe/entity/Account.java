/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.o7spring5restjpamysqlbe.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author nleit_000
 */
@Entity
//@Table(name = "account", catalog = "o7", schema = "")
@Table(name = "account")
@NamedQueries({
    @NamedQuery(name = "Account.findAll", query = "SELECT a FROM Account a")
    , @NamedQuery(name = "Account.findByAccountId", query = "SELECT a FROM Account a WHERE a.accountId = :accountId")
    , @NamedQuery(name = "Account.findByAvailBalance", query = "SELECT a FROM Account a WHERE a.availBalance = :availBalance")
    , @NamedQuery(name = "Account.findByCloseDate", query = "SELECT a FROM Account a WHERE a.closeDate = :closeDate")
    , @NamedQuery(name = "Account.findByLastActivityDate", query = "SELECT a FROM Account a WHERE a.lastActivityDate = :lastActivityDate")
    , @NamedQuery(name = "Account.findByOpenDate", query = "SELECT a FROM Account a WHERE a.openDate = :openDate")
    , @NamedQuery(name = "Account.findByPendingBalance", query = "SELECT a FROM Account a WHERE a.pendingBalance = :pendingBalance")
    , @NamedQuery(name = "Account.findByStatus", query = "SELECT a FROM Account a WHERE a.status = :status")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Account implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ACCOUNT_ID")
    private Integer accountId;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "AVAIL_BALANCE")
    private Float availBalance;
    
    @Column(name = "CLOSE_DATE")
    @Temporal(TemporalType.DATE)
    private Date closeDate;
   
    @Column(name = "LAST_ACTIVITY_DATE")
    @Temporal(TemporalType.DATE)
    private Date lastActivityDate;
    
    @Basic(optional = false)
    @Column(name = "OPEN_DATE")
    @Temporal(TemporalType.DATE)
    private Date openDate;
    
    @Column(name = "PENDING_BALANCE")
    private Float pendingBalance;
   
    @Column(name = "STATUS")
    private String status;
    
    @OneToMany(mappedBy = "accountId")
    @JsonIgnore
    private List<AccTransaction> accTransactionList;
    
    @JoinColumn(name = "OPEN_BRANCH_ID", referencedColumnName = "BRANCH_ID")
    @ManyToOne(optional = false)
    private Branch openBranchId;
    
    @JoinColumn(name = "CUST_ID", referencedColumnName = "CUST_ID")
    @ManyToOne
    private Customer custId;
    
    @JoinColumn(name = "OPEN_EMP_ID", referencedColumnName = "EMP_ID")
    @ManyToOne(optional = false)
    private Employee openEmpId;
    
    @JoinColumn(name = "PRODUCT_CD", referencedColumnName = "PRODUCT_CD")
    @ManyToOne(optional = false)
    private Product productCd;

    public Account() {
    }

    public Account(Integer accountId) {
        this.accountId = accountId;
    }

    public Account(Integer accountId, Date openDate) {
        this.accountId = accountId;
        this.openDate = openDate;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Float getAvailBalance() {
        return availBalance;
    }

    public void setAvailBalance(Float availBalance) {
        this.availBalance = availBalance;
    }

    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    public Date getLastActivityDate() {
        return lastActivityDate;
    }

    public void setLastActivityDate(Date lastActivityDate) {
        this.lastActivityDate = lastActivityDate;
    }

    public Date getOpenDate() {
        return openDate;
    }

    public void setOpenDate(Date openDate) {
        this.openDate = openDate;
    }

    public Float getPendingBalance() {
        return pendingBalance;
    }

    public void setPendingBalance(Float pendingBalance) {
        this.pendingBalance = pendingBalance;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<AccTransaction> getAccTransactionList() {
        return accTransactionList;
    }

    public void setAccTransactionList(List<AccTransaction> accTransactionList) {
        this.accTransactionList = accTransactionList;
    }

    public Branch getOpenBranchId() {
        return openBranchId;
    }

    public void setOpenBranchId(Branch openBranchId) {
        this.openBranchId = openBranchId;
    }

    public Customer getCustId() {
        return custId;
    }

    public void setCustId(Customer custId) {
        this.custId = custId;
    }

    public Employee getOpenEmpId() {
        return openEmpId;
    }

    public void setOpenEmpId(Employee openEmpId) {
        this.openEmpId = openEmpId;
    }

    public Product getProductCd() {
        return productCd;
    }

    public void setProductCd(Product productCd) {
        this.productCd = productCd;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (accountId != null ? accountId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Account)) {
            return false;
        }
        Account other = (Account) object;
        if ((this.accountId == null && other.accountId != null) || (this.accountId != null && !this.accountId.equals(other.accountId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.myapp.entity.Account[ accountId=" + accountId + " ]";
    }
    
}
