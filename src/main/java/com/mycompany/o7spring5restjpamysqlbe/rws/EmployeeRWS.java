package com.mycompany.o7spring5restjpamysqlbe.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.o7spring5restjpamysqlbe.entity.Employee;
import com.mycompany.o7spring5restjpamysqlbe.service.EmployeeService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class EmployeeRWS 
{
	private static final Logger logger = LoggerFactory.getLogger(EmployeeRWS.class);
	
	@Autowired
	private EmployeeService employeeService;
	
	@GetMapping("/employees/count")
	public ResponseEntity<Long> cout()
	{
		return new ResponseEntity<Long>(employeeService.count(), HttpStatus.OK);
	}
	
	@GetMapping("/employee/{id}")
	public ResponseEntity<Employee>  getOne(@PathVariable("id") Integer id)
	{
		logger.info("Listing entity with id: " + id); 
		return new ResponseEntity<Employee>(employeeService.getOne(id), HttpStatus.OK);
	}
	
	@GetMapping("/employees/")
	public ResponseEntity<List<Employee>> findAll()
	{
		logger.info("Listing all entities"); 
		return new ResponseEntity<List<Employee>>(employeeService.findAll(), HttpStatus.OK);
	}
	
	@GetMapping("/employees/page/{pageNum}")
	public ResponseEntity<Page<Employee>> findPage(@PathVariable("pageNum") Integer pageNum)
	{
		logger.info("Listing page: " + pageNum); 
		return new ResponseEntity<Page<Employee>>(employeeService.findPage(pageNum), HttpStatus.OK);
	}
	
	@PostMapping("/employee")
	public ResponseEntity<Employee> save(@RequestBody Employee emp)
	{
		logger.info("Creating Employee"); 	
		try
		{
			Employee empRet = employeeService.save(emp);
			logger.info("Done");
			return new ResponseEntity<Employee>(empRet, HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Employee>(HttpStatus.BAD_REQUEST);					
		}
	}
	
	@PutMapping("/employee/{id}")
	public ResponseEntity<Employee> update(@PathVariable Integer id, @RequestBody Employee emp)
	{				
		logger.info("Editing Employee"); 	
		try
		{
			Employee empRet = employeeService.edit(id, emp);
			logger.info("Done");
			return new ResponseEntity<Employee>(empRet, HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while editing the entity");
			return new ResponseEntity<Employee>(HttpStatus.BAD_REQUEST);					
		}
	}

}
