package com.mycompany.o7spring5restjpamysqlbe.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.o7spring5restjpamysqlbe.entity.AccTransaction;
import com.mycompany.o7spring5restjpamysqlbe.entity.Account;
import com.mycompany.o7spring5restjpamysqlbe.service.AccTransactionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class AccTransactionRWS 
{
	private static final Logger logger = LoggerFactory.getLogger(AccTransactionRWS.class);
	
	@Autowired
	private AccTransactionService accTransactionService;
	
	@GetMapping("/acctransaction/count")
	public ResponseEntity<Long> cout()
	{
		return new ResponseEntity<Long>(accTransactionService.count(), HttpStatus.OK);
	}
	
	@GetMapping("/acctransaction/{id}")
	public ResponseEntity<AccTransaction> getOne(@PathVariable("id") Long id)
	{
		logger.info("Listing entity with id: " + id); 
		return new ResponseEntity<AccTransaction>(accTransactionService.getOne(id), HttpStatus.OK);
	}
	
	@GetMapping("/acctransactions")
	public ResponseEntity<List<AccTransaction>> findAll()
	{
		logger.info("Listing all entities"); 
		return new ResponseEntity<List<AccTransaction>>(accTransactionService.findAll(), HttpStatus.OK);
	}

	@GetMapping("/acctransactions/{page}")
	public ResponseEntity<Page<AccTransaction>> findAllWithPaging(@PathVariable("page") Integer page)
	{
		logger.info("Listing all entities of page: " + page); 
		return new ResponseEntity<Page<AccTransaction>>(accTransactionService.findAllWithPaging(page), HttpStatus.OK);
	}
	
	@PostMapping("/acctransaction")
	public ResponseEntity<AccTransaction> save(@RequestBody AccTransaction at)
	{
		logger.info("Creating Acc Transaction"); 	
		try
		{
			AccTransaction aRet = accTransactionService.save(at);
			logger.info("Done");
			return new ResponseEntity<AccTransaction>(aRet, HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<AccTransaction>(HttpStatus.BAD_REQUEST);					
		}
	}
	
	@PutMapping("/acctransaction/{id}")
	public ResponseEntity<AccTransaction> update(@PathVariable Long id, @RequestBody AccTransaction at)
	{				
		logger.info("Editing Acc Transaction"); 	
		try
		{
			AccTransaction atRet = accTransactionService.edit(id, at);
			logger.info("Done");
			return new ResponseEntity<AccTransaction>(atRet, HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while editing the entity");
			return new ResponseEntity<AccTransaction>(HttpStatus.BAD_REQUEST);					
		}
	}
}
