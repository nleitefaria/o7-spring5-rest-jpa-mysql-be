package com.mycompany.o7spring5restjpamysqlbe.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.o7spring5restjpamysqlbe.entity.Business;
import com.mycompany.o7spring5restjpamysqlbe.service.BusinessService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class BusinessRWS 
{
	private static final Logger logger = LoggerFactory.getLogger(BusinessRWS.class);
	
	@Autowired
	private BusinessService businessService;
	
	@GetMapping("/business/count")
	public ResponseEntity<Long> cout()
	{
		return new ResponseEntity<Long>(businessService.count(), HttpStatus.OK);
	}
	
	@GetMapping("/business/{id}")
	public ResponseEntity<Business> getOne(@PathVariable("id") Integer id)
	{
		logger.info("Listing entity with id: " + id); 
		return new ResponseEntity<Business>(businessService.getOne(id), HttpStatus.OK);
	}
	
	@GetMapping("/businesses")
	public ResponseEntity<List<Business>> findAll()
	{
		logger.info("Listing all entities"); 
		return new ResponseEntity<List<Business>>(businessService.findAll(), HttpStatus.OK);
	}
	
	@GetMapping("/businesses/page/{pageNum}")
	public ResponseEntity<Page<Business>> findPage(@PathVariable("pageNum") Integer pageNum)
	{
		logger.info("Listing page: " + pageNum); 
		return new ResponseEntity<Page<Business>>(businessService.findPage(pageNum), HttpStatus.OK);
	}
	
	@PostMapping("/business")
	public ResponseEntity<Business> save(@RequestBody Business b)
	{
		logger.info("Creating Business"); 	
		try
		{
			Business bRet = businessService.save(b);
			logger.info("Done");
			return new ResponseEntity<Business>(bRet, HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Business>(HttpStatus.BAD_REQUEST);					
		}
	}
	
	@PutMapping("/business/{id}")
	public ResponseEntity<Business> update(@PathVariable Integer id, @RequestBody Business b)
	{				
		logger.info("Editing Product type"); 	
		try
		{
			Business bRet = businessService.edit(id, b);
			logger.info("Done");
			return new ResponseEntity<Business>(bRet, HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while editing the entity");
			return new ResponseEntity<Business>(HttpStatus.BAD_REQUEST);					
		}
	}

}
