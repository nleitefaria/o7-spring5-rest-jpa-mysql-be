package com.mycompany.o7spring5restjpamysqlbe.rws;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.mycompany.o7spring5restjpamysqlbe.entity.Account;
import com.mycompany.o7spring5restjpamysqlbe.service.AccountService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class AccountRWS
{
	private static final Logger logger = LoggerFactory.getLogger(AccountRWS.class);
	
	@Autowired
	private AccountService accountService;
	
	@GetMapping("/account/count")
	public ResponseEntity<Long> cout()
	{
		return new ResponseEntity<Long>(accountService.count(), HttpStatus.OK);
	}
	
	@GetMapping("/account/{id}")
	public ResponseEntity<Account> getOneAccount(@PathVariable("id") Integer id)
	{
		logger.info("Listing entity with id: " + id); 
		return new ResponseEntity<Account>(accountService.getOne(id), HttpStatus.OK);
	}
	
	@GetMapping("/accounts/page/{pageNum}")
	public ResponseEntity<Page<Account>> findPage(@PathVariable("pageNum") Integer pageNum)
	{
		logger.info("Listing page: " + pageNum); 
		return new ResponseEntity<Page<Account>>(accountService.findPage(pageNum), HttpStatus.OK);
	}
	
	@GetMapping("/accounts")
	public ResponseEntity<List<Account>> findAll()
	{
		logger.info("Listing all entities"); 
		return new ResponseEntity<List<Account>>(accountService.findAll(), HttpStatus.OK);
	}
		
	@PostMapping("/account")
	public ResponseEntity<Account> save(@RequestBody Account a)
	{
		logger.info("Creating Account"); 	
		try
		{
			Account aRet = accountService.save(a);
			logger.info("Done");
			return new ResponseEntity<Account>(aRet, HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Account>(HttpStatus.BAD_REQUEST);					
		}
	}
	
	@PutMapping("/account/{id}")
	public ResponseEntity<Account> update(@PathVariable Integer id, @RequestBody Account a)
	{				
		logger.info("Editing Account"); 	
		try
		{
			Account acRet = accountService.edit(id, a);
			logger.info("Done");
			return new ResponseEntity<Account>(acRet, HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while editing the entity");
			return new ResponseEntity<Account>(HttpStatus.BAD_REQUEST);					
		}
	}
	
	
	
	
	

}
