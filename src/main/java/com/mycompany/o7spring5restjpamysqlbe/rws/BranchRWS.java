package com.mycompany.o7spring5restjpamysqlbe.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.o7spring5restjpamysqlbe.entity.Branch;
import com.mycompany.o7spring5restjpamysqlbe.service.BranchService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class BranchRWS
{	
	private static final Logger logger = LoggerFactory.getLogger(BranchRWS.class);
	
	@Autowired
	private BranchService branchService;
	
	@GetMapping("/branch/count")
	public ResponseEntity<Long> cout()
	{
		return new ResponseEntity<Long>(branchService.count(), HttpStatus.OK);
	}
	
	@GetMapping("/branch/{id}")
	public ResponseEntity<Branch> getOne(@PathVariable("id") Integer id)
	{
		logger.info("Listing entity with id: " + id); 
		return new ResponseEntity<Branch>(branchService.getOne(id), HttpStatus.OK);
	}
	
	@GetMapping("/branches")
	public ResponseEntity<List<Branch>> findAll()
	{
		logger.info("Listing all entities"); 
		return new ResponseEntity<List<Branch>>(branchService.findAll(), HttpStatus.OK);
	}
	
	@GetMapping("/branches/{page}")
	public ResponseEntity<Page<Branch>> findAllWithPaging(@PathVariable("page") Integer page)
	{
		logger.info("Listing all entities of page: " + page); 
		return new ResponseEntity<Page<Branch>>(branchService.findAllWithPaging(page), HttpStatus.OK);
	}

}
