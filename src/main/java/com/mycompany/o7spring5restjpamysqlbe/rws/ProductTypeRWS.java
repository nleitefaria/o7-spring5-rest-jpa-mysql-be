package com.mycompany.o7spring5restjpamysqlbe.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.o7spring5restjpamysqlbe.entity.ProductType;
import com.mycompany.o7spring5restjpamysqlbe.service.ProductTypeService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ProductTypeRWS
{
	private static final Logger logger = LoggerFactory.getLogger(ProductTypeRWS.class);
	
	@Autowired
	private ProductTypeService productTypeService;
	
	@GetMapping("/producttype/count")
	public ResponseEntity<Long> cout()
	{
		return new ResponseEntity<Long>(productTypeService.count(), HttpStatus.OK);
	}
	
	@GetMapping("/producttype/{id}")
	public ResponseEntity<ProductType> getOne(@PathVariable("id") String id)
	{
		logger.info("Listing entity with id: " + id); 
		return new ResponseEntity<ProductType>(productTypeService.getOne(id), HttpStatus.OK);
	}
	
	@GetMapping("/producttypes/page/{pageNum}")
	public ResponseEntity<Page<ProductType>> findPage(@PathVariable("pageNum") Integer pageNum)
	{
		logger.info("Listing page: " + pageNum); 
		return new ResponseEntity<Page<ProductType>>(productTypeService.findPage(pageNum), HttpStatus.OK);
	}
	
	@GetMapping("/producttypes")
	public ResponseEntity<List<ProductType>> findAll()
	{
		logger.info("Listing all entities"); 
		return new ResponseEntity<List<ProductType>>(productTypeService.findAll(), HttpStatus.OK);
	}
	
	@PostMapping("/producttype")
	public ResponseEntity<ProductType> save(@RequestBody ProductType pt)
	{
		logger.info("Creating Product type"); 	
		try
		{
			ProductType ptRet = productTypeService.save(pt);
			logger.info("Done");
			return new ResponseEntity<ProductType>(ptRet, HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<ProductType>(HttpStatus.BAD_REQUEST);					
		}
	}
	
	@PutMapping("/producttype/{id}")
	public ResponseEntity<ProductType> update(@PathVariable String id, @RequestBody ProductType pt)
	{				
		logger.info("Editing Product type"); 	
		try
		{
			ProductType ptRet = productTypeService.edit(id, pt);
			logger.info("Done");
			return new ResponseEntity<ProductType>(ptRet, HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while editing the entity");
			return new ResponseEntity<ProductType>(HttpStatus.BAD_REQUEST);					
		}
	}

}
