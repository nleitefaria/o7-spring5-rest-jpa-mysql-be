package com.mycompany.o7spring5restjpamysqlbe.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.o7spring5restjpamysqlbe.entity.Individual;
import com.mycompany.o7spring5restjpamysqlbe.service.IndividualService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class IndividualRWS 
{
	private static final Logger logger = LoggerFactory.getLogger(IndividualRWS.class);
	
	@Autowired
	private IndividualService individualService;
	
	@GetMapping("/individuals/count")
	public ResponseEntity<Long> cout()
	{
		return new ResponseEntity<Long>(individualService.count(), HttpStatus.OK);
	}
	
	@GetMapping("/individual/{id}")
	public ResponseEntity<Individual>  getOne(@PathVariable("id") Integer id)
	{
		return new ResponseEntity<Individual>(individualService.getOne(id), HttpStatus.OK);
	}
	
	@GetMapping("/individuals/")
	public ResponseEntity<List<Individual>> findAll()
	{
		return new ResponseEntity<List<Individual>>(individualService.findAll(), HttpStatus.OK);
	}
	
	@GetMapping("/individuals/page/{pageNum}")
	public ResponseEntity<Page<Individual>> findPage(@PathVariable("pageNum") Integer pageNum)
	{
		logger.info("Listing page: " + pageNum); 
		return new ResponseEntity<Page<Individual>>(individualService.findPage(pageNum), HttpStatus.OK);
	}
	
	@PostMapping("/individual")
	public ResponseEntity<Individual> save(@RequestBody Individual ind)
	{
		logger.info("Creating Individual"); 	
		try
		{
			Individual indRet = individualService.save(ind);
			logger.info("Done");
			return new ResponseEntity<Individual>(indRet, HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Individual>(HttpStatus.BAD_REQUEST);					
		}
	}
	
	@PutMapping("/individual/{id}")
	public ResponseEntity<Individual> update(@PathVariable Integer id, @RequestBody Individual ind)
	{				
		logger.info("Editing Individual"); 	
		try
		{
			Individual indRet = individualService.edit(id, ind);
			logger.info("Done");
			return new ResponseEntity<Individual>(indRet, HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while editing the entity");
			return new ResponseEntity<Individual>(HttpStatus.BAD_REQUEST);					
		}
	}
}
