package com.mycompany.o7spring5restjpamysqlbe.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.o7spring5restjpamysqlbe.entity.Department;
import com.mycompany.o7spring5restjpamysqlbe.service.DepartmentService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class DepartmentRWS 
{
	private static final Logger logger = LoggerFactory.getLogger(DepartmentRWS.class);
	
	@Autowired
	private DepartmentService departmentService;
	
	@GetMapping("/department/count")
	public ResponseEntity<Long> cout()
	{
		return new ResponseEntity<Long>(departmentService.count(), HttpStatus.OK);
	}
	
	@GetMapping("/department/{id}")
	public ResponseEntity<Department> getOne(@PathVariable("id") Integer id)
	{
		logger.info("Listing entity with id: " + id); 
		return new ResponseEntity<Department>(departmentService.getOne(id), HttpStatus.OK);
	}
	
	@GetMapping("/departments")
	public ResponseEntity<List<Department>> findAll()
	{
		logger.info("Listing all entities"); 
		return new ResponseEntity<List<Department>>(departmentService.findAll(), HttpStatus.OK);
	}
	
	@GetMapping("/departments/page/{pageNum}")
	public ResponseEntity<Page<Department>> findPage(@PathVariable("pageNum") Integer pageNum)
	{
		logger.info("Listing page: " + pageNum); 
		return new ResponseEntity<Page<Department>>(departmentService.findPage(pageNum), HttpStatus.OK);
	}
	
	@PostMapping("/department")
	public ResponseEntity<Department> save(@RequestBody Department dep)
	{
		logger.info("Creating Department"); 	
		try
		{
			Department depRet = departmentService.save(dep);
			logger.info("Done");
			return new ResponseEntity<Department>(depRet, HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Department>(HttpStatus.BAD_REQUEST);					
		}
	}
	
	@PutMapping("/department/{id}")
	public ResponseEntity<Department> update(@PathVariable Integer id, @RequestBody Department pt)
	{				
		logger.info("Editing Department"); 	
		try
		{
			Department depRet = departmentService.edit(id, pt);
			logger.info("Done");
			return new ResponseEntity<Department>(depRet, HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while editing the entity");
			return new ResponseEntity<Department>(HttpStatus.BAD_REQUEST);					
		}
	}
	
}
