package com.mycompany.o7spring5restjpamysqlbe.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.o7spring5restjpamysqlbe.entity.Customer;
import com.mycompany.o7spring5restjpamysqlbe.service.CustomerService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class CustomerRWS 
{
	private static final Logger logger = LoggerFactory.getLogger(BusinessRWS.class);
	
	@Autowired
	private CustomerService customerService;
	
	@GetMapping("/customer/count")
	public ResponseEntity<Long> cout()
	{
		return new ResponseEntity<Long>(customerService.count(), HttpStatus.OK);
	}
	
	@GetMapping("/customer/{id}")
	public ResponseEntity<Customer> getOne(@PathVariable("id") Integer id)
	{
		logger.info("Listing entity with id: " + id); 
		return new ResponseEntity<Customer>(customerService.getOne(id), HttpStatus.OK);
	}
	
	@GetMapping("/customers")
	public ResponseEntity<List<Customer>> findAll()
	{
		logger.info("Listing all entities"); 
		return new ResponseEntity<List<Customer>>(customerService.findAll(), HttpStatus.OK);
	}
	
	@GetMapping("/customers/page/{pageNum}")
	public ResponseEntity<Page<Customer>> findPage(@PathVariable("pageNum") Integer pageNum)
	{
		logger.info("Listing page: " + pageNum); 
		return new ResponseEntity<Page<Customer>>(customerService.findPage(pageNum), HttpStatus.OK);
	}
	
	@PostMapping("/customer")
	public ResponseEntity<Customer> save(@RequestBody Customer c)
	{
		logger.info("Creating Customer"); 	
		try
		{
			Customer cRet = customerService.save(c);
			logger.info("Done");
			return new ResponseEntity<Customer>(cRet, HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Customer>(HttpStatus.BAD_REQUEST);					
		}
	}
	
	@PutMapping("/customer/{id}")
	public ResponseEntity<Customer> update(@PathVariable Integer id, @RequestBody Customer c)
	{				
		logger.info("Editing Customer"); 	
		try
		{
			Customer cRet = customerService.edit(id, c);
			logger.info("Done");
			return new ResponseEntity<Customer>(cRet, HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while editing the entity");
			return new ResponseEntity<Customer>(HttpStatus.BAD_REQUEST);					
		}
	}
	

}
