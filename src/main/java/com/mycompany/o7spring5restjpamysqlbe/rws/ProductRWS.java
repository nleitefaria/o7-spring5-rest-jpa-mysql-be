package com.mycompany.o7spring5restjpamysqlbe.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.o7spring5restjpamysqlbe.entity.Product;
import com.mycompany.o7spring5restjpamysqlbe.service.ProductService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ProductRWS
{
	private static final Logger logger = LoggerFactory.getLogger(ProductRWS.class);
	
	@Autowired
	private ProductService productService;
	
	@GetMapping("/product/count")
	public ResponseEntity<Long> cout()
	{
		return new ResponseEntity<Long>(productService.count(), HttpStatus.OK);
	}
	
	@GetMapping("/product/{id}")
	public ResponseEntity<Product> getOne(@PathVariable("id") String id)
	{
		logger.info("Listing entity with id: " + id); 
		return new ResponseEntity<Product>(productService.getOne(id), HttpStatus.OK);
	}
	
	@GetMapping("/products/page/{pageNum}")
	public ResponseEntity<Page<Product>> findPage(@PathVariable("pageNum") Integer pageNum)
	{
		logger.info("Listing page: " + pageNum); 
		return new ResponseEntity<Page<Product>>(productService.findPage(pageNum), HttpStatus.OK);
	}
	
	@GetMapping("/products")
	public ResponseEntity<List<Product>> findAll()
	{
		logger.info("Listing all entities"); 
		return new ResponseEntity<List<Product>>(productService.findAll(), HttpStatus.OK);
	}
	
	@PostMapping("/product")
	public ResponseEntity<Product> save(@RequestBody Product p)
	{
		logger.info("Creating Product type"); 	
		try
		{
			Product pRet = productService.save(p);
			logger.info("Done");
			return new ResponseEntity<Product>(pRet, HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Product>(HttpStatus.BAD_REQUEST);					
		}
	}
	
	@PutMapping("/product/{id}")
	public ResponseEntity<Product> update(@PathVariable String id, @RequestBody Product p)
	{				
		logger.info("Editing Product type"); 	
		try
		{
			Product pRet = productService.edit(id, p);
			logger.info("Done");
			return new ResponseEntity<Product>(pRet, HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while editing the entity");
			return new ResponseEntity<Product>(HttpStatus.BAD_REQUEST);					
		}
	}

}
