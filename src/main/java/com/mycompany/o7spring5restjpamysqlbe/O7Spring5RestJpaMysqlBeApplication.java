package com.mycompany.o7spring5restjpamysqlbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class O7Spring5RestJpaMysqlBeApplication {

	public static void main(String[] args) {
		SpringApplication.run(O7Spring5RestJpaMysqlBeApplication.class, args);
	}
}
