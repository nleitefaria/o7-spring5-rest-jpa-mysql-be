package com.mycompany.o7spring5restjpamysqlbe.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.mycompany.o7spring5restjpamysqlbe.entity.Customer;

public interface CustomerService
{
	Long count();
	Customer getOne(Integer id);
	List<Customer> findAll();
	Page<Customer> findPage(int page);
	Customer save(Customer c);
	Customer edit(Integer id, Customer c);

}
