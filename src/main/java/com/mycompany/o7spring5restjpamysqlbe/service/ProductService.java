package com.mycompany.o7spring5restjpamysqlbe.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.mycompany.o7spring5restjpamysqlbe.entity.Product;

public interface ProductService
{
	Long count();
	Product getOne(String id);
	Page<Product> findPage(int page);
	List<Product> findAll();
	Product save(Product p);
	Product edit(String id, Product p);
	void deleteAll();
}
