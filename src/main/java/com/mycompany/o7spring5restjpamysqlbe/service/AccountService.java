package com.mycompany.o7spring5restjpamysqlbe.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.mycompany.o7spring5restjpamysqlbe.entity.Account;

public interface AccountService 
{	
	Long count();
	Account getOne(Integer id);
	Page<Account> findPage(int page);
	List<Account> findAll();
	Account edit(Integer id, Account a);
	Account save(Account a);
	void deleteAll();
}
