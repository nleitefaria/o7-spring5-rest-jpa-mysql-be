package com.mycompany.o7spring5restjpamysqlbe.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.mycompany.o7spring5restjpamysqlbe.entity.ProductType;

public interface ProductTypeService
{
	Long count();
	ProductType getOne(String id);
	List<ProductType> findAll();
	Page<ProductType> findPage(int page);
	ProductType save(ProductType pt);
	ProductType edit(String id, ProductType pt);
	void deleteAll();
}
