package com.mycompany.o7spring5restjpamysqlbe.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.o7spring5restjpamysqlbe.entity.Product;
import com.mycompany.o7spring5restjpamysqlbe.entity.ProductType;
import com.mycompany.o7spring5restjpamysqlbe.repository.ProductTypeRepository;
import com.mycompany.o7spring5restjpamysqlbe.service.ProductTypeService;

@Service
public class ProductTypeServiceImpl implements ProductTypeService
{
	@Autowired
	private ProductTypeRepository productTypeRepository;
	
	@Transactional
	public Long count()
	{
		return productTypeRepository.count();
	}
	
	@Transactional
	public ProductType getOne(String id)
	{
		return productTypeRepository.getOne(id);
	}
	
	@Transactional
	public List<ProductType> findAll()
	{
		return productTypeRepository.findAll();		
	}
	
	@Transactional
	public Page<ProductType> findPage(int page)
	{
		return productTypeRepository.findAll(gotoPage(page-1));
	}
	
	@Transactional
	public ProductType save(ProductType pt)
	{
		return productTypeRepository.save(pt);	
	}
	
	@Transactional
	public ProductType edit(String id, ProductType pt)
	{	
		ProductType ptEdit = productTypeRepository.getOne(id);
		ptEdit.setName(pt.getName());
		return productTypeRepository.save(ptEdit);		
	}
	
	@Transactional
	public void deleteAll()
	{
		productTypeRepository.deleteAll();
	}
	
	private PageRequest gotoPage(int page)
    {
		PageRequest request = new PageRequest(page, 10);        
        return request;
    }
}
