package com.mycompany.o7spring5restjpamysqlbe.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.o7spring5restjpamysqlbe.entity.Department;
import com.mycompany.o7spring5restjpamysqlbe.repository.DepartmentRepository;
import com.mycompany.o7spring5restjpamysqlbe.service.DepartmentService;

@Service
public class DepartmentServiceImpl implements DepartmentService
{
	@Autowired
	private DepartmentRepository departmentRepository;
	
	@Transactional
	public Long count()
	{
		return departmentRepository.count();
	}
	
	@Transactional
	public Department getOne(Integer id)
	{
		return departmentRepository.getOne(id);
	}
	
	@Transactional
	public List<Department> findAll()
	{
		return departmentRepository.findAll();		
	}
	
	@Transactional
	public Page<Department> findPage(int page)
	{
		return departmentRepository.findAll(gotoPage(page-1));
	}
	
	@Transactional
	public Department save(Department dep)
	{
		return departmentRepository.save(dep);	
	}
	
	@Transactional
	public Department edit(Integer id, Department dep)
	{	
		Department depEdit = departmentRepository.getOne(id);
		depEdit.setName(dep.getName());
		return departmentRepository.save(depEdit);		
	}
	
	@Transactional
	public void deleteAll()
	{	
		departmentRepository.deleteAll();
	}
	
	private PageRequest gotoPage(int page)
    {
		PageRequest request = new PageRequest(page, 10);        
        return request;
    }

}
