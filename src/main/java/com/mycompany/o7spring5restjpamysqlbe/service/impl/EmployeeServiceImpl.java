package com.mycompany.o7spring5restjpamysqlbe.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.o7spring5restjpamysqlbe.entity.Employee;
import com.mycompany.o7spring5restjpamysqlbe.repository.EmployeeRepository;
import com.mycompany.o7spring5restjpamysqlbe.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService
{
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Transactional
	public Long count()
	{
		return employeeRepository.count();
	}
	
	@Transactional
	public Employee getOne(Integer id)
	{
		return employeeRepository.getOne(id);
	}
	
	@Transactional
	public List<Employee> findAll()
	{
		return employeeRepository.findAll();		
	}
	
	@Transactional
	public Page<Employee> findPage(int page)
	{
		return employeeRepository.findAll(gotoPage(page-1));
	}
	
	@Transactional
	public Employee save(Employee e)
	{
		return employeeRepository.save(e);	
	}
	
	@Transactional
	public Employee edit(Integer id, Employee e)
	{	
		Employee eEdit = employeeRepository.getOne(id);
		
		if(e.getFirstName() != null)
		{
			eEdit.setFirstName(e.getFirstName());			
		}
		
		if(e.getLastName() != null)
		{
			eEdit.setLastName(e.getLastName());			
		}
			
		return employeeRepository.save(eEdit);		
	}
	
	@Transactional
	public void deleteAll()
	{
		employeeRepository.deleteAll();
	}
	
	private PageRequest gotoPage(int page)
    {
		PageRequest request = new PageRequest(page, 10);        
        return request;
    }

}
