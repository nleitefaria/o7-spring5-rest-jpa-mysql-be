package com.mycompany.o7spring5restjpamysqlbe.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.o7spring5restjpamysqlbe.entity.AccTransaction;
import com.mycompany.o7spring5restjpamysqlbe.repository.AccTransactionRepository;
import com.mycompany.o7spring5restjpamysqlbe.service.AccTransactionService;

@Service
public class AccTransactionServiceImpl implements AccTransactionService 
{
	@Autowired
	private AccTransactionRepository accTransactionRepository;
	
	public Long count()
	{
		return accTransactionRepository.count();
	}
	
	public AccTransaction getOne(Long id)
	{
		return accTransactionRepository.getOne(id);
	}
	
	public List<AccTransaction> findAll()
	{
		return accTransactionRepository.findAll();		
	}
	
	public Page<AccTransaction> findAllWithPaging(int page)
	{
		return accTransactionRepository.findAll(createPageRequest(page));	
	}
	
	@Transactional
	public AccTransaction save(AccTransaction at)
	{
		return accTransactionRepository.save(at);	
	}
	
	@Transactional
	public AccTransaction edit(Long id, AccTransaction at)
	{	
		AccTransaction accTransaction = accTransactionRepository.getOne(id);
		accTransaction.setAmount(at.getAmount());
		return accTransactionRepository.save(accTransaction);		
	}
	
	@Transactional
	public void deleteAll()
	{
		accTransactionRepository.deleteAll();
	}
	
	private Pageable createPageRequest(int page)
	{
	    return new PageRequest(page - 1, 10);
	}
	

}
