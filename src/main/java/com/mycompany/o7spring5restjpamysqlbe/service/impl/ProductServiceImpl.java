package com.mycompany.o7spring5restjpamysqlbe.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.o7spring5restjpamysqlbe.entity.Product;
import com.mycompany.o7spring5restjpamysqlbe.repository.ProductRepository;
import com.mycompany.o7spring5restjpamysqlbe.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService
{
	@Autowired
	private ProductRepository productRepository;
	
	@Transactional
	public Long count()
	{
		return productRepository.count();
	}
	
	@Transactional
	public Product getOne(String id)
	{
		return productRepository.getOne(id);
	}
	
	@Transactional
	public List<Product> findAll()
	{
		return productRepository.findAll();		
	}
	
	@Transactional
	public Page<Product> findPage(int page)
	{
		return productRepository.findAll(gotoPage(page-1));
	}
	
	@Transactional
	public Product save(Product p)
	{
		return productRepository.save(p);	
	}
	
	@Transactional
	public Product edit(String id, Product p)
	{	
		Product ptEdit = productRepository.getOne(id);
		ptEdit.setName(p.getName());
		return productRepository.save(ptEdit);		
	}
	
	@Transactional
	public void deleteAll()
	{
		productRepository.deleteAll();
	}
	
	private PageRequest gotoPage(int page)
    {
		PageRequest request = new PageRequest(page, 10);        
        return request;
    }
}
