package com.mycompany.o7spring5restjpamysqlbe.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.o7spring5restjpamysqlbe.entity.Business;
import com.mycompany.o7spring5restjpamysqlbe.repository.BusinessRepository;
import com.mycompany.o7spring5restjpamysqlbe.service.BusinessService;

@Service
public class BusinessServiceImpl implements BusinessService
{
	@Autowired
	private BusinessRepository businessRepository;
	
	@Transactional
	public Long count()
	{
		return businessRepository.count();
	}
	
	@Transactional
	public Business getOne(Integer id)
	{
		return businessRepository.getOne(id);
	}
	
	@Transactional
	public List<Business> findAll()
	{
		return businessRepository.findAll();		
	}
	
	@Transactional
	public Page<Business> findPage(int page)
	{
		return businessRepository.findAll(gotoPage(page-1));
	}
	
	@Transactional
	public Business save(Business b)
	{
		return businessRepository.save(b);	
	}
	
	@Transactional
	public Business edit(Integer id, Business b)
	{	
		Business bEdit = businessRepository.getOne(id);
		bEdit.setName(b.getName());
		return businessRepository.save(bEdit);		
	}
	
	@Transactional
	public void deleteAll()
	{
		businessRepository.deleteAll();
	}
	
	private PageRequest gotoPage(int page)
    {
		PageRequest request = new PageRequest(page, 10);        
        return request;
    }

}
