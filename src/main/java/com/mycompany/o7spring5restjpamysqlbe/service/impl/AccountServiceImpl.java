package com.mycompany.o7spring5restjpamysqlbe.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.o7spring5restjpamysqlbe.entity.Account;
import com.mycompany.o7spring5restjpamysqlbe.repository.AccountRepository;
import com.mycompany.o7spring5restjpamysqlbe.service.AccountService;

@Service
public class AccountServiceImpl implements  AccountService
{
	@Autowired
	private AccountRepository accountRepository;
	
	@Transactional
	public Long count()
	{
		return accountRepository.count();
	}
	
	@Transactional
	public Account getOne(Integer id)
	{
		return accountRepository.getOne(id);
	}
	
	@Transactional
	public Page<Account> findPage(int page)
	{
		return accountRepository.findAll(gotoPage(page-1));
	}
	
	@Transactional
	public List<Account> findAll()
	{
		return accountRepository.findAll();		
	}
	
	@Transactional
	public Account save(Account a)
	{
		return accountRepository.save(a);	
	}
	
	@Transactional
	public Account edit(Integer id, Account a)
	{	
		Account acEdit = accountRepository.getOne(id);
		acEdit.setStatus(a.getStatus());
		return accountRepository.save(acEdit);		
	}
	
	@Transactional
	public void deleteAll()
	{
		accountRepository.deleteAll();
	}
	
	
	private PageRequest gotoPage(int page)
    {
		PageRequest request = new PageRequest(page, 10);        
        return request;
    }
}
