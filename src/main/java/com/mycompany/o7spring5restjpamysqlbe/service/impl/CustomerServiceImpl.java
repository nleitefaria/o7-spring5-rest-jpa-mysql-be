package com.mycompany.o7spring5restjpamysqlbe.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.o7spring5restjpamysqlbe.entity.Customer;
import com.mycompany.o7spring5restjpamysqlbe.repository.CustomerRepository;
import com.mycompany.o7spring5restjpamysqlbe.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService
{
	@Autowired
	private CustomerRepository customerRepository;
	
	@Transactional
	public Long count()
	{
		return customerRepository.count();
	}
	
	@Transactional
	public Customer getOne(Integer id)
	{
		return customerRepository.getOne(id);
	}
	
	@Transactional
	public List<Customer> findAll()
	{
		return customerRepository.findAll();		
	}
	
	@Transactional
	public Page<Customer> findPage(int page)
	{
		return customerRepository.findAll(gotoPage(page-1));
	}
	
	@Transactional
	public Customer save(Customer c)
	{
		return customerRepository.save(c);	
	}
	
	@Transactional
	public Customer edit(Integer id, Customer c)
	{	
		Customer cEdit = customerRepository.getOne(id);
		cEdit.setAddress(c.getAddress());
		return customerRepository.save(cEdit);		
	}
	
	@Transactional
	public void deleteAll()
	{
		customerRepository.deleteAll();
	}	
	
	private PageRequest gotoPage(int page)
    {
		PageRequest request = new PageRequest(page, 10);        
        return request;
    }
}
