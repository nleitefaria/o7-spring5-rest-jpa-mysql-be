package com.mycompany.o7spring5restjpamysqlbe.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.o7spring5restjpamysqlbe.entity.Branch;
import com.mycompany.o7spring5restjpamysqlbe.repository.BranchRepository;
import com.mycompany.o7spring5restjpamysqlbe.service.BranchService;

@Service
public class BranchServiceImpl implements BranchService 
{
	@Autowired
	private BranchRepository branchRepository;
	
	@Transactional
	public Long count()
	{
		return branchRepository.count();
	}
	
	@Transactional
	public Branch getOne(Integer id)
	{
		return branchRepository.getOne(id);
	}
	
	@Transactional
	public List<Branch> findAll()
	{
		return branchRepository.findAll();		
	}
	
	@Transactional
	public Page<Branch> findAllWithPaging(int page)
	{
		return branchRepository.findAll(gotoPage(page));	
	}
	
	@Transactional
	public Branch save(Branch b)
	{
		return branchRepository.save(b);	
	}
	
	@Transactional
	public Branch edit(Integer id, Branch b)
	{	
		Branch bEdit = branchRepository.getOne(id);
		
		System.out.println("+++++++++++++++++");
		System.out.println(bEdit);
		System.out.println("+++++++++++++++++");
		
		System.out.println("*****************");
		System.out.println("----> " + bEdit.getName());
		System.out.println("*****************");
		
		bEdit.setName(b.getName());
		return branchRepository.save(bEdit);		
	}
	
	@Transactional
	public void deleteAll()
	{
		branchRepository.deleteAll();
	}
	
	private PageRequest gotoPage(int page)
    {
		PageRequest request = new PageRequest(page, 10);        
        return request;
    }

}
