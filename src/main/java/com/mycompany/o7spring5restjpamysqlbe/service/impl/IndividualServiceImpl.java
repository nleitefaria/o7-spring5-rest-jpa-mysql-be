package com.mycompany.o7spring5restjpamysqlbe.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.o7spring5restjpamysqlbe.entity.Individual;
import com.mycompany.o7spring5restjpamysqlbe.repository.IndividualRepository;
import com.mycompany.o7spring5restjpamysqlbe.service.IndividualService;

@Service
public class IndividualServiceImpl implements IndividualService
{
	@Autowired
	private IndividualRepository individualRepository;
	
	@Transactional
	public Long count()
	{
		return individualRepository.count();
	}
	
	@Transactional
	public Individual getOne(Integer id)
	{
		return individualRepository.getOne(id);
	}
	
	@Transactional
	public List<Individual> findAll()
	{
		return individualRepository.findAll();		
	}
	
	@Transactional
	public Page<Individual> findPage(int page)
	{
		return individualRepository.findAll(gotoPage(page-1));
	}
	
	@Transactional
	public Individual save(Individual i)
	{
		return individualRepository.save(i);	
	}
	
	@Transactional
	public Individual edit(Integer id, Individual i)
	{	
		Individual iEdit = individualRepository.getOne(id);
		
		if(i.getFirstName() != null)
		{
			iEdit.setFirstName(i.getFirstName());			
		}
		
		if(i.getLastName() != null)
		{
			iEdit.setLastName(i.getLastName());			
		}
			
		return individualRepository.save(iEdit);		
	}
	
	@Transactional
	public void deleteAll()
	{
		individualRepository.deleteAll();
	}
	
	private PageRequest gotoPage(int page)
    {
		PageRequest request = new PageRequest(page, 10);        
        return request;
    }
}
