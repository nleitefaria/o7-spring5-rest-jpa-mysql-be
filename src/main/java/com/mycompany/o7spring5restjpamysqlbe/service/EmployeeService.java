package com.mycompany.o7spring5restjpamysqlbe.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.mycompany.o7spring5restjpamysqlbe.entity.Employee;

public interface EmployeeService 
{
	Long count();
	Employee getOne(Integer id);
	List<Employee> findAll();
	Page<Employee> findPage(int page);
	Employee save(Employee e);
	Employee edit(Integer id, Employee e);
	void deleteAll();

}
