package com.mycompany.o7spring5restjpamysqlbe.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.mycompany.o7spring5restjpamysqlbe.entity.Department;

public interface DepartmentService {
	
	Long count();
	Department getOne(Integer id);
	List<Department> findAll();
	Page<Department> findPage(int page);
	Department save(Department dep);
	Department edit(Integer id, Department dep);
	void deleteAll();

}
