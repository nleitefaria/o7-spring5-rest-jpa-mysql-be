package com.mycompany.o7spring5restjpamysqlbe.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.mycompany.o7spring5restjpamysqlbe.entity.AccTransaction;

public interface AccTransactionService 
{	
	Long count();
	AccTransaction getOne(Long id);
	List<AccTransaction> findAll();
	Page<AccTransaction> findAllWithPaging(int page);
	AccTransaction save(AccTransaction at);
	AccTransaction edit(Long id, AccTransaction at);
}
