package com.mycompany.o7spring5restjpamysqlbe.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.mycompany.o7spring5restjpamysqlbe.entity.Business;

public interface BusinessService
{	
	Long count();
	Business getOne(Integer id);
	List<Business> findAll();
	Page<Business> findPage(int page);
	Business save(Business b);
	Business edit(Integer id, Business b);
	void deleteAll();

}
