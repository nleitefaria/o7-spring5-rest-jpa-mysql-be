package com.mycompany.o7spring5restjpamysqlbe.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.mycompany.o7spring5restjpamysqlbe.entity.Branch;

public interface BranchService 
{
	 Long count();
	 Branch getOne(Integer id);
	 List<Branch> findAll();
	 Page<Branch> findAllWithPaging(int page);
	 Branch save(Branch b);
	 Branch edit(Integer id, Branch b);
	 void deleteAll();

}
