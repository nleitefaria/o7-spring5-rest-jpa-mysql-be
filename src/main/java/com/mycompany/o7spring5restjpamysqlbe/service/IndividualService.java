package com.mycompany.o7spring5restjpamysqlbe.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.mycompany.o7spring5restjpamysqlbe.entity.Individual;

public interface IndividualService 
{
	Long count();
	Individual getOne(Integer id);
	List<Individual> findAll();
	Page<Individual> findPage(int page);
	Individual save(Individual i);
	Individual edit(Integer id, Individual i);
	void deleteAll();

}
