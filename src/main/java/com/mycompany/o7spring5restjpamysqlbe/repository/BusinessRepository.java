package com.mycompany.o7spring5restjpamysqlbe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.o7spring5restjpamysqlbe.entity.Business;

@Repository
public interface BusinessRepository extends JpaRepository<Business, Integer> {

}
