package com.mycompany.o7spring5restjpamysqlbe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.mycompany.o7spring5restjpamysqlbe.entity.Branch;

@Repository
public interface BranchRepository extends JpaRepository<Branch, Integer> {

}
